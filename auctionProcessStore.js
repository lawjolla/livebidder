var spawn = require("child_process").spawn;
var env = Object.create(process.env);
const copartWsAddress$ = require("./helpers/copartWebsocketAddress");

const processPath = {
  copart: "./copart/copart.js"
};


class childProcessesStore {
  constructor() {
    this.childProcesses = {};
    this.copartWsAddress = "";
    copartWsAddress$.subscribe(address => this.updateCopartWsAddress(address));
  }
  isAlreadyRunning(username) {
    return this.childProcesses.hasOwnProperty(username);
  }
  updateCopartWsAddress(address) {
    if (address.includes("http")) {
      this.copartWsAddress = address;
    }
  }
  add(username, password) {
    const wsAddress = this.copartWsAddress;
    if (!wsAddress.includes("http")) {
      console.log("Copart Websocket Address Not Set");
      setTimeout(() => this.add(username, password), 5000);
      return false;
    }
    if (this.isAlreadyRunning(username)) {
      return false;
    }
    //return false;
    env.username = username;
    env.password = password;
    env.wsAddress = wsAddress;
    this.childProcesses[username] = spawn("node", [processPath["copart"]], {
      env
    });
    //this.childProcesses[username].stdin.write("Hello there!");
    this.childProcesses[username].stdout.on("data", function(data) {
      console.log(data.toString());
    });
    this.childProcesses[username].stderr.on("data", data => {
      console.log(Date().toString() + " Error! " + data.toString());
      if (data.toString().toUpperCase().includes("UNEXPECTED TOKEN")) {
        // unknown, non JSON..  response, probably server error
        console.log(
          Date().toString() + ": JSON Error, Will Restart in One Hour"
        );
        setTimeout(() => this.restart(username, password), 3600000);
      } else {
        setTimeout(() => this.restart(username, password), 5000);
      }
    });
  }
  restart(username, password) {
    this.kill(username);
    this.add(username, password);
  }
  kill(username) {
    this.childProcesses[username].kill();
    delete this.childProcesses[username];
  }
  killAll() {
    Object.keys(this.childProcesses).forEach(username => this.kill(username));
  }
  get store() {
    return this.childProcesses;
  }
}

module.exports = new childProcessesStore();
