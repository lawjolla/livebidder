var fetch = require('node-fetch');
var Rx = require('@reactivex/rxjs');

var clientId = false;
function fetch$(url, body) {
  //const body = '[{"channel":"/meta/connect","connectionType":"long-polling","id":"403","clientId":"zx1zc59qafbvpbctly8izryk7uz"}]'
  return Rx.Observable.create(observer => {
      fetch(url, {method: 'POST', headers: { 'Content-Type': 'application/json',  "user-agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",}, body: body })
      .then(result => result.json())
      .then(result => {  if (result.hasOwnProperty('clientId')) { clientId = result.clientId; } return observer.next(result) })
      .then(result => observer.complete())
      .catch(error => observer.error({error: true, errorMsg: error}));
  })
};

const fetchStream$ =
  fetch$('http://cometd.webclient.iaa4.auctionsolutions.com/comet-ams/comet/handshake', '[{"version":"1.0","minimumVersion":"0.9","channel":"/meta/handshake","supportedConnectionTypes":["long-polling","callback-polling"],"advice":{"timeout":60000,"interval":0},"id":"1"}]')
    .switchMap(() => fetch$('http://cometd.webclient.iaa4.auctionsolutions.com/comet-ams/comet/', '[{"channel":"/meta/subscribe","subscription":"/topic/attend-response-endpoint","id":"2","clientId":"'+clientId+'"}]'))
    .switchMap(() => fetch$('http://cometd.webclient.iaa4.auctionsolutions.com/comet-ams/comet/', '[{"channel":"/service/queue/stateMachine.Queue","data":{"headers":{"operation":"create","AMSLANE":"iaaphoenix151b","AMSROLE":null,"AMSSALENO":"62069","AHCO":"iaai","COMETSESSIONID":"1hei1zybwpis838199ek9dp5uyqn"},"body":"saleno=62069|userAgent=TW96aWxsYS81LjAgKE1hY2ludG9zaDsgSW50ZWwgTWFjIE9TIFggMTBfMTJfMSkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzU1LjAuMjg4My4wIFNhZmFyaS81MzcuMzY=|flavor=iaa2012|lang=en|password=3050554113714c24375|eventid=PR82540_2|displaylocation=Tucson-AZ|lane=iaaphoenix151b|username=182208_294375|launchType=web|deviceType=Macintosh|httpSessionId=186bd2d6-b6c8-4757-a858-98cf34cf791a|filename=weben|ahco=iaai|attendId=|displayname=182208|ipAddress=69.244.8.88"},"id":"4","clientId":"1hei1zybwpis838199ek9dp5uyqn"}]'))
    .switchMap(() => fetch$('http://cometd.webclient.iaa4.auctionsolutions.com/comet-ams/comet/connect', '[{"channel":"/meta/connect","connectionType":"long-polling","id":"8","clientId":"'+clientId+'"}]'))
    .repeat()
  .take(5)
//  .share();

module.exports = fetchStream$;