#!/usr/bin/env bash
cd repos/livebidder
pm2 stop index.json
git fetch --all
git checkout --force "origin/production"
npm install
prisma deploy
pm2 start deploy.json --restart-delay 60000
