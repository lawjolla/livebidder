var Rx = require('@reactivex/rxjs');
var url = require('../copart/copartAuth/copartUrls');
var request$ = require('../copart/copartAuth/copartRequest');
const polling = 18000000; // 5 hours
const wsAddress$ =
    Rx.Observable.interval(polling)
        .startWith(0)
        .switchMap(() => request$(url.copartWsOptions))
        // .do(x => {
        //     if (typeof x == 'object') console.log('copartWsObject', x);
        //     if (typeof x == 'string') console.log('copartWsString', x);
        //     return x;
        // })
        .map(x => JSON.parse(x))
        .map(x => x.data.reference.nirvanaWebHosts[0])
        .do(x => console.log('WS', x))
        .publishReplay(1)
        .refCount();

module.exports = wsAddress$;