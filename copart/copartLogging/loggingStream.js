//var filteredCopartDataStream$ = require('./filteredCopartDataStream');
var dealerConstants = require('../copartStores/dealerConstants');
var copartWebSocketStream$ = require('../copartWebsocket/copartWebSocketStream').copartStream$;

var loggingStream$ = copartWebSocketStream$
  .groupBy(stream => stream.auction)
  .mergeMap(stream =>
    stream.scan((acc, cur) => {
      if (cur.tag === 'CURITEM') {
      acc = [];
    }
    acc.push(cur);
    return acc;
    }, [])
  .filter(stream => stream[stream.length-1].tag === 'SOLD'));

var weBid$ = loggingStream$.filter(didWeBid);

function didWeBid(stream) {
  //console.log('Did we bid? ', stream.findIndex(x => x.data.BUYERNO == dealerConstants.copart.username) > -1);
  return stream.findIndex(x => x.data.BUYERNO == dealerConstants.copart.username) > -1
}

module.exports = loggingStream$;