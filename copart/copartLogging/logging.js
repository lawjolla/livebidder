var dealer = require('../copartStores/dealerConstants');
var fetch = require('node-fetch');

function logBid(x) {
  const soldFrame = x[x.length-1].data;
  let body = {
    auction: 'Copart', lotNo: parseInt(soldFrame.LOTNO), currentBid: parseInt(soldFrame.BID),
    rawBidData: x, wonAuction: 0, hasBid: 1
  };
  if (soldFrame.BUYERNO == dealer.copart.username) body.wonAuction = 1;
  console.log(`Auction Result for ${soldFrame.LOTNO}: ${soldFrame.BUYERNO}`);
  fetch('http://www.wheelkinetics.com/buy/api/result', { method: 'POST', body: JSON.stringify(body)})
    .catch(err => console.log('Logging Error: ',err))
}

module.exports = logBid;