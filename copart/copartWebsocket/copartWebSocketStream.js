var WebSocket = require('reconnecting-websocket');
var nodeWs = require('ws');
var Rx = require('@reactivex/rxjs');
var subscribedAuctions = require('./../copartStores/copartSubscribedAuctionStore');
var auth$ = require('./../copartAuth/copartBidToken');
var dealer = require('./../copartStores/dealerConstants');
var copartStream$ = new Rx.Subject();
var request$ = require('../copartAuth/copartRequest');
var url = require('../copartAuth/copartUrls');
//var cookie = require('cookie');

var pingCount = 1;
function getPing() {
  pingCount++;
  return pingCount;
}

const wsUrl = process.env.wsAddress || 'http://nirvanalv204.copart.com:80';
const options = { constructor: nodeWs, maxRetries: 50};
var ws = new WebSocket(wsUrl, null, options);
ws.on('open', () => {
  console.log('Websocket Connected To Copart');
  keepAlive();
  auth$.subscribe(auth => {
    const connectionString = 'F=1&Connection-Type=JC&Y=9&V=Netscape&P=nws&W=Build-12638&X=May-1 2014&Z=MacIntel&S='+auth.token+'&A=VB3&G=F&D=T&B=&R='+getPing()+'&1Date=' + Math.floor(new Date()) + '&';
    ws.send(connectionString);
  }, error => console.log('auth error', error), () => console.log('auth complete'))
});

ws.on('error', (err) => {
  switch(err.code) {
    case 'ETIMEDOUT' :
      console.log('WS Error: ETIMEDOUT' );
      break;
    case 'ENOTFOUND' :
      console.log('WS Error: ENOTFOUND' );
      break;
    case 'EHOSTDOWN' :
      console.log('Server Down');
    default:
      break;
  }
  console.log('WS Error: ', err)
});
ws.on('message', function(data, flags) {
  // flags.binary will be set if a binary data is received.
  // flags.masked will be set if the data was masked.
  const jsonData = JSON.parse(data)[0];
  const parseData = readData(JSON.parse(data))[0];
  if (jsonData.hasOwnProperty('r')) {
   // console.log(data);
    checkForNewAuction(jsonData);
  }
  if (typeof parseData !== 'undefined') {
    const frameData = JSON.parse(decodeBase64(parseData.data));
    const tag = decodeBase64(parseData.tag);
    const convertedData = convertStringsToInt(frameData);
    //console.log('Tag: ' + tag);
    copartStream$.next({data: convertedData, tag: tag, auction: parseData.auction });
  }
});

ws.on('close', (e) => {
  console.log('WS close', e);
  if (e == 1006) {
    throw Error('Websocket Closed on 1006.  Restart');
  }
});

function checkForNewAuction(jsonData) {
  if (jsonData.hasOwnProperty('d')) {
    if (Array.isArray(jsonData.d[1]) && jsonData.d[1][0].includes('outbound')) {
      const auctionKey = jsonData.d[1][0].split('/')[1];
      const streamKey = jsonData.d[1][2];
      subscribedAuctions.addStreamKey(auctionKey, streamKey);
      console.log('Subscribed To Lane: ' + auctionKey + ' with key: ' + streamKey);
    }
  }
}

function keepAlive() {
  setInterval(() => {
    ws.send('F=3&R=' + getPing() + '&', (err) => {
      if (err !== undefined) {
        console.log('Send Error', err);
      }
  });
  },59000);
}

function decodeBase64(string) {
  return new Buffer(string, 'base64').toString('utf8');
}


function readData(frame) {
  return frame
      .filter(obj => obj.hasOwnProperty('d'))
      .filter(obj => obj['d'][1].hasOwnProperty('Data'))
      .map(obj => ({ data: obj['d'][1]['Data'], tag: obj['d'][1]['Tag'], auction: obj['d'][1]['CNAME'] }));
}

function convertStringsToInt(data) {
  const ints = ['CURBID', 'NEXT', 'LOTNO', 'BUYERNO'];
  ints.forEach(int => {
    if (data.hasOwnProperty(int)) {
      data[int] = parseInt(data[int]);
    }
  });
  return data;
}

exports.copartWs = ws;
exports.getPing = getPing;
exports.copartStream$ = copartStream$;


