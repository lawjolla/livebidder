'use strict';

var fetch = require('node-fetch');
var Rx = require('@reactivex/rxjs');

const url = 'http://www.wheelkinetics.com/buy/copart/exportBids.php';

const polling = 60000;


function fetch$(url) {
  return Rx.Observable.create(observer => {
    fetch(url)
     .then(result => result.json())
     .then(result => observer.next(result))
     .then(result => observer.complete())
     .catch(error => observer.error({error: true, errorMsg: error}));
  })
};


const ourBids$ = 
  Rx.Observable.interval(polling)
  .startWith(0)
  .switchMap(() => fetch$(url))
 // .do(x => console.log('ourbids', x))
  .retryWhen(errors =>
      errors.scan((errorCount, err) => {
        if(errorCount >= 2) {
          throw err;
        }
        return errorCount + 1;
    }, 0))
  //.do(x => console.log('Checked for new cars to bid'))
  .publishReplay(1)
  .refCount();

  // .retryWhen(err => {
  //   console.log('Error Our Bids: ', err.errorMsg)
  //   return err.delay(polling).take(3);
  // })

module.exports = ourBids$;
