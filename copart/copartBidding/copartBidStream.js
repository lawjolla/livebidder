var copartWebSocketStream$ = require('../copartWebsocket/copartWebSocketStream').copartStream$;
var ourBids$ = require('./ourBids');
var authToken$ = require('../copartAuth/copartBidToken');
var dealer = require('../copartStores/dealerConstants');
var Rx = require('@reactivex/rxjs');

const copartBidStream$ = ourBids$
 // .filter(bids => bids.length > 0)
  .combineLatest(copartWebSocketStream$, (ourBids, bidStream) => ({ourBids: ourBids, bidStream: bidStream.data, tag: bidStream.tag}))
  .combineLatest(authToken$, (stream, authToken) => ({ ourBids: stream.ourBids, bidStream: stream.bidStream, tag: stream.tag, auth: authToken.token }))
  .do(x => {
      if (x.tag === 'LOGINRESP') {
        console.log('LOGIN RESPONSE: ', x.bidStream);
      }
      if (x.tag === 'CURITEM') {
        console.log('New Item: ' + x.bidStream.DESCRIPT +', At: ' + x.bidStream.YARDNAME + ', Number: ' + x.bidStream.ITEMNO);
      }
   })
  .filter(stream => (stream.tag === 'BIDREC' || stream.tag === 'STARTITEM' ))
  .filter(bidOnThisLot)
  .do(x => {
      if (x.tag === 'BIDREC') {
        console.log('Bid Stream: ' + x.bidStream.CURBID + ', ' + x.bidStream.BUYERNO);
      }
  })
  .filter(isNotOurBidderNumber)
  .do(x => {
    console.log('Passed Not Our Bidder Number: ' + x.bidStream.BUYERNO);
    console.log('Checking is our bid higher than current bid: ' + x.bidStream.LOTNO + ', Bid: ' + x.bidStream.NEXT);
  })
  .filter(isOurBidHigherThanCurrentBid)
  .do(x => {
    console.log('Passed Is Our Bid Higher Than Current Bid Check: ' + x.bidStream.LOTNO);
  })
  .debounce(x => Rx.Observable.of(true).delay(x.bidStream.TYPE === 'P' ? 100 : Math.round(Math.random() * 1000 + 1000)))
  .do(x => {
    console.log('*** BID ***');
    console.log('Our Bid: ' + x.bidStream.NEXT);
  })



function isOurBidHigherThanCurrentBid(stream) {
  const { ourBids, bidStream } = stream;
  const ourBid = ourBids.find(bid => bid.lotNo === bidStream.LOTNO);
  return ourBid.bidTo >= bidStream.NEXT;
}
function isNotOurBidderNumber(stream) {
  //console.log('Bidder No: ' + stream.bidStream.BUYERNO);
  return parseInt(stream.bidStream.BUYERNO) !== parseInt(dealer.copart.username);
}
function bidOnThisLot(stream) {
  const { ourBids, bidStream } = stream;
  return ourBids.some(bid => bid.lotNo === bidStream.LOTNO)
}

module.exports = copartBidStream$;


// bid that worked
// F=14&R=31&N=/COPART062B/inbound&T=Ymlk&D=eyJMT1ROTyI6MjUxMDQ3OTYsIkFNT1VOVCI6MTI4MDAsIlRZUEUiOiJJIiwiQlVZRVJUT0tFTiI6IkNLRTVCWkY1M1kifQ==&L=0
// nid that did not work
// F=14&R=17&N=/COPART062B/inbound&T=Ymlk&D=eyJMT1ROTyI6MjUxMDQ3OTYsIkFNT1VOVCI6MTMwMDAsIlRZUEUiOiJJIiwiQlVZRVJUT0tFTiI6IldDRjROWDVSVlcifQ==&L=0
