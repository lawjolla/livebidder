var copartWebSocket = require('../copartWebsocket/copartWebSocketStream').copartWs;
var getPing = require('../copartWebsocket/copartWebSocketStream').getPing;


function copartBidOnVehicle(stream) {
  const { ourBids, bidStream, auth } = stream;
  const ourBid = ourBids.find(bid => bid.lotNo === bidStream.LOTNO);
  const lane = `COPART${pad(ourBid.yard, 3)}${ourBid.lane}`;
  const bidPacket = {"LOTNO": ourBid.lotNo, "AMOUNT": bidStream.NEXT, "TYPE": "I", "BUYERTOKEN": auth.toString()};
  //console.log(bidPacket);
  var encodedBidPacket = new Buffer(JSON.stringify(bidPacket)).toString('base64');
  const bidString = `F=14&R=${getPing()}&N=/${lane}/inbound&T=Ymlk&D=${encodedBidPacket}&L=0`;
  // F=14&R=12&N=/COPART078E/inbound&T=Ymlk&D=eyJMT1ROTyI6MzMzOTk5MjcsIkFNT1VOVCI6MTc1LCJUWVBFIjoiSSIsIkJVWUVSVE9LRU4iOiJFU1FBWTZXS1hUIn0=&L=0
  // {"LOTNO":33399927,"AMOUNT":175,"TYPE":"I","BUYERTOKEN":"ESQAY6WKXT"}
  copartWebSocket.send(bidString);

}


function pad(num, size) {
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}

module.exports = copartBidOnVehicle;
