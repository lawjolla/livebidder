const chrome = require(`./chrome.js`)

const USERNAME_SELECTOR = `#username`
const PASSWORD_SELECTOR = `#password`
const BUTTON_SELECTOR = `#show > div.col-xs-12.pull-right.padding0 > button`

const CREDS = {
  username: `110554`, password: `Republic02`
}
let cookies = []

const login = async (browser) => {
  const url = `https://www.copart.com/login`
  const page = await browser()
  await page.goto(url)

  await page.click(USERNAME_SELECTOR)
  await page.keyboard.type(CREDS.username)

  await page.click(PASSWORD_SELECTOR)
  await page.keyboard.type(CREDS.password)

  await page.click(BUTTON_SELECTOR)
  await page.waitForNavigation()
  const cook = await page.cookies()
  cookies = [...cook]
  return true
};

const page1 = async (browser) => {
  const url = `https://www.copart.com/paymentsDue/`
  const page = await browser()
  page.setCookie(...cookies)
  await page.goto(url)
  const x = await page.content()
  console.log('x',x)
  // await page.waitForNavigation()
  await page.screenshot({ path: `screenshot1.png` })
  return true
}
// login(chrome)
login(chrome).then(() => page1(chrome)).catch(err => console.log('errrr', err))
// getPage(`https://www.copart.com/login/`)
//  .then(res => res.screenshot({ path: `screenshot.png` }))
//   .then(() => {
//     setTimeout(() => getPage(`https://www.copart.com/dashboard/`), 5000)
//   })