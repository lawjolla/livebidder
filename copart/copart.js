//var Rx = require('@reactivex/rxjs');
var liveCopartAuctions$ = require('./copartAuth/copartLiveSalesWithReadyBids');
var subscribedAuctions = require('./copartStores/copartSubscribedAuctionStore');
var ourBids$ = require('./copartBidding/ourBids');

var loggingStream$ = require('./copartLogging/loggingStream');
var logger = require('./copartLogging/logging');
var copartBidOnVehicle = require('./copartBidding/copartBidOnVehicle');
var copartBidStream$ = require('./copartBidding/copartBidStream');
//var iaaStream$ = require('./iaaLongPollStream');

// iaaStream$.subscribe(x => console.log(x), (e) => console.error(e), () => console.log('complete'));
 const copartBidStreamRef$ =
   copartBidStream$.subscribe(x => copartBidOnVehicle(x));

const copartSubscriptionStream$ = ourBids$
  .combineLatest(liveCopartAuctions$, (ourBids, auctions) => combineAuctions(ourBids, auctions))
  .subscribe(x => {
    subscribedAuctions.currentAuctions(x.ourAuctions);
  })

const logOurBidding = loggingStream$
  .withLatestFrom(ourBids$)
  .map(([stream, bids]) => ({ stream, bids }))
  .filter(didWeBid)
  .do(x => console.log('we bid!'))
  .subscribe(x => logger(x.stream));

function didWeBid(x) {
  const { stream, bids } = x;
  const lotNo = stream[stream.length-1].data.LOTNO;
  return bids.findIndex(bid => bid.lotNo == lotNo) > -1
}

function combineAuctions(ourBids, auctions) {
   const ourAuctions = auctions.filter(auction => filterAuctions(ourBids, auction));
   function filterAuctions(ourBid, auction) {
    const yard = parseInt(auction.replace(/^\D+/g, ''));
    const lane = auction.slice(-1);
    return ourBid.reduce((acc, bid) => {
      if (yard === bid.yard && lane === bid.lane) {
        acc = true;
      }
      return acc;
     }, false);
    }
   return ({ ourAuctions: ourAuctions });
}