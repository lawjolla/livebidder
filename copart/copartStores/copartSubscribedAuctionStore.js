'use strict';

var subscribedAuctions = module.exports = {
  subscriptions: {},
  addStreamKey: function(key, streamKey) {
    subscribedAuctions.subscriptions[key]['streamKey'] = streamKey;
  },
  addAuction: function(key, streamKey = '', auctionToken = '') {
    //console.log('Added ' + key, this.auctions);
    if (!subscribedAuctions.subscriptions.hasOwnProperty(key)) {
      subscribedAuctions.subscriptions[key] = {key, streamKey, auctionToken };
      copartSubscribeToAuctionLane(key);
    }
  },
  currentAuctions: function(currentAuctionsVar) {
    const auctionsNow = subscribedAuctions.subscriptions;
    const auctionKeys = Object.keys(auctionsNow);
    auctionKeys.forEach(auctionSubscribed => {
      if (currentAuctionsVar.findIndex(auction => auction === auctionSubscribed) === -1) {
        subscribedAuctions.removeAuction(auctionSubscribed)
      }
    })

    currentAuctionsVar.forEach(auctionToAdd => {
      if (auctionKeys.findIndex(auctionsSubscribed => auctionsSubscribed === auctionToAdd) === -1) {
        console.log('Add Auction: ' + auctionToAdd);
         subscribedAuctions.addAuction(auctionToAdd);
      }
    })
   },
  removeAuction: function(auction) {
    delete subscribedAuctions.subscriptions[auction];
    copartWebSocket.send(`F=15&R=${getPing()}&N=/${auction}/outbound`);
  },
  updateAuctionToken: function(token, key) {
    subscribedAuctions.subscriptions[key]['auctionToken'] = token;
  }
};

var copartSubscribeToAuctionLane = require('../copartAuth/copartSubscribeToAuctionLane');
var copartWebSocket = require('../copartWebsocket/copartWebSocketStream').copartWs;
var getPing = require('../copartWebsocket/copartWebSocketStream').getPing;