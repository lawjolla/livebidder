var headers = require('./copartRequestHeaders');
const domain = 'https://www.copart.com';
const auctionDomain = 'http://auction.copart.com/c3';
const username = process.env.username || 110554;
const password = process.env.password || 'Republic02';

const copartFrontPage = {
  url: 'https://www.copart.com',
  method: 'GET',
  headers,
  followAllRedirects: true,
};

const copartLogin = {
  url: `${domain}/processLogin`,
  method: 'POST',
  headers,
  json: {
    username, password,
    accountType: { value: 0, description: 'Member'},
    accountTypeValue: 0,
    rememberme: true
  },
  followAllRedirects: true
};

const copartAuctionDashboard = {
  url: `${domain}/data/auctionDashboard`,
  method: 'GET',
  headers,
  followAllRedirects: true,
};

const copartPublicAuctionDashboard = {
  url: `${domain}/public/data/auctionDashboard`,
  method: 'GET',
  headers,
  followAllRedirects: true,
};

const copartAuctionHTML = {
  url: `${auctionDomain}/auctions.html?appId=G2&siteLanguage=en&appId=g2`,
  method: 'GET',
  headers,
  followAllRedirects: true,
};

const copartAuctionBidToken = {
  url: `${auctionDomain}/data/user/details?_=${Math.floor(new Date())}`,
  method: 'GET',
  headers,
  followAllRedirects: true,
};

const copartUserConfig = {
  url: `${domain}/public/data/userConfig`, // ?_=${Math.floor(new Date())}
  method: 'GET',
  headers: {
    'Host': 'www.copart.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:53.0) Gecko/20100101 Firefox/53.0',
    'Accept-Language': 'en-US,en;q=0.5',

    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
  },
  followAllRedirects: true,
};

const copartAuctionLaneToken = (auction) => {
  return {
    url: `${auctionDomain}/data/sale/login/${auction}/C3US?_=${Math.floor(new Date())}000`,
    method: 'GET',
    headers,
    followAllRedirects: true,
  }
};

const copartSalesList = {
    url: `${auctionDomain}/data/sales/list/G2?_=${Math.floor(new Date())}000`,
    method: 'GET',
    headers,
    followAllRedirects: true,
};

const copartWsOptions = {
  url: `${auctionDomain}/data/app/reference`, // ?_=${Math.floor(new Date())}000`
  method: 'GET',
  headers,
  followAllRedirects: true,
}


module.exports = {
  copartFrontPage,
  copartLogin,
  copartAuctionDashboard,
  copartPublicAuctionDashboard,
  copartAuctionHTML,
  copartAuctionBidToken,
  copartUserConfig,
  copartAuctionLaneToken,
  copartSalesList,
  copartWsOptions
}
