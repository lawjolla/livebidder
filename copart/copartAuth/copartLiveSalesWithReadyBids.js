var Rx = require('@reactivex/rxjs');
var url = require('./copartUrls');
var request$ = require('./copartRequest');
const polling = 100000;

const currentAuctions$ =
  Rx.Observable.interval(polling)
    .startWith(0)
    .switchMap(() => request$(url.copartSalesList))
   // .do(x => console.log('copartlivesaleswithreadybids HERE', x))
    .map(auctions => JSON.parse(auctions))
    .map(auctions => auctions.data.saleList.liveSales)
    .map(auctions => auctions.filter(auction => Date.now() > (auction.saleUtcDateTime - polling))) // live sales
    .map(auctions => auctions.map(auction => auction.auctionKey))
    //.do(x => console.log('New Auctions Complete'))

module.exports = currentAuctions$;