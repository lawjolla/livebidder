var Rx = require('@reactivex/rxjs');
var url = require('./copartUrls');
var request$ = require('./copartRequest');
var login$ = require('./copartLogin');

var $getBidToken = Rx.Observable.concat(
 // request$(url.copartAuctionDashboard),
  request$(url.copartPublicAuctionDashboard),
  //request$(url.copartAuctionHTML),
  request$(url.copartAuctionBidToken)
);

var loginAndGetBidToken$ = Rx.Observable.concat(
  login$,
  $getBidToken
);

const auth2$ =
  // request$(url.copartFrontPage)
 // .switchMap(x => request$(url.copartUserConfig))
  //.switchMap(x => loginAndGetBidToken$)
    loginAndGetBidToken$
  // .map(x => {
  //   if (x && JSON.parse(x) && JSON.parse(x).data) {
  //     return x;
  //   }
  //   throw "ex";
  // })
  // .retryWhen(errors => {
  //   return errors.scan((errorCount, err) => {
  //     if(errorCount >= 5) {
  //       console.log("Unable to get past incapsula");
  //       throw err;
  //     }
  //     return errorCount + 1;
  //   }, 0).delay(Math.round(Math.random() * 1000) + 2000)
  // })
  // .switchMap(x => JSON.parse(x).data.userConfig.buyerNumber === 1
  //   ? loginAndGetBidToken$
  //   : $getBidToken
  // )
  .last()
  .do(x => console.log('BID', x))
  .map(x => ({token: JSON.parse(x).data.userDetails.buyerToken}))
  .do(x => console.log('Bid token: ' +x.token))
  .publishReplay(1)
  .refCount();

  // .subscribe(
  //   (x) => auth$.next(x),
  //   (e) => auth$.error({err: true, errMsg: e})
  // );

module.exports = auth2$;