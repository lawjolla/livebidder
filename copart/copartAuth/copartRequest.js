var Rx = require("@reactivex/rxjs");
var request = require("request");
var headers = require("./copartRequestHeaders");
var copartCookieJar = request.jar();
const browser = require("../browser/chrome")

request = request.defaults({ jar: copartCookieJar });

const USERNAME_SELECTOR = `#username`
const PASSWORD_SELECTOR = `#password`
const BUTTON_SELECTOR = `#show > div.col-xs-12.pull-right.padding0 > button`
const REMEMBER_ME_SELECTOR = `#show > div:nth-child(3) > div:nth-child(1) > input`

const CREDS = {
  username: process.env.username || 110554,
  password: process.env.password || 'Republic02'
}

function log(options, logOutput) {
  if (!options.url.includes(`sales/list`)) {
    console.log(logOutput)
  }
}

let cookies = []

const login = async () => {
    const page = await browser()
    const url = `https://www.copart.com/login`
    page.setCookie(...cookies)
    await page.goto(url)

    await page.click(USERNAME_SELECTOR)
    await page.keyboard.type(CREDS.username)

    await page.click(PASSWORD_SELECTOR)
    await page.keyboard.type(CREDS.password)

    await page.click(REMEMBER_ME_SELECTOR)

    await page.click(BUTTON_SELECTOR)
    await page.waitForNavigation()
    const cook = await page.cookies()
    page.setCookie(...cook)
    cookies = [...cook]

}
function request$(options) {
  log(options, Date().toString() + ": " + options.url);
  return Rx.Observable.create(async observer => {
    const page = await browser()
    page.setCookie(...cookies)
    if (options.url.includes(`Login`)) {
      await login()
      observer.next();
      observer.complete();
    }
    await page.goto(options.url)
    const result = await page.evaluate(() =>  {
      return document.querySelector(`body`).innerText
    });
    const cook = await page.cookies()
    cookies = [...cook]
    //console.log(options.url, result)
    if (result.toUpperCase().includes(`/INCAPSULA`)) {
      console.log('incapsula')
      observer.error(`Incapsula test`)
    }
    observer.next(result);
    observer.complete();
    // await page.waitForNavigation()
    // await page.screenshot({path: `screenshot1.png`})
  })
}

function request$1(options) {
  return Rx.Observable.create(observer => {
    if (!options.url.includes("sales/list"))
      log(options, Date().toString() + ": " + options.url);
    request(options, (err, res, body) => {
      if (err) {
        log(options, "request error", err, options.url);
        observer.error(err);
      }
      observer.next(body);
      observer.complete();
    });
  });
}


function incapsulaLink(b) {
  var code = b.match(/b="(.+?)"/i)[1];
  var z = "";
  for (var i = 0; i < code.length; i += 2) {
    z = z + String.fromCharCode(parseInt(code.substring(i, i + 2), 16));
  }
  var xhr = z.match(/xhr.ope(.+?);/)[1];
  var link = xhr.match(/"\/(.+?)"/)[1];
  return "/" + link;
}

function checkForIncapsulaBlock(data, options) {
  return Rx.Observable.create(obs => {
    log(options, `${Date().toString()}: INCAP BLOCKED CHECK ${options.url}`)
    if (JSON.stringify(data).toUpperCase().includes("INCAPSULA INCIDENT ID")) {
      log(options, `${Date().toString()}: INCAP BLOCKED ${options.url}`)
      obs.error(data);
    } else {
      log(options, `${Date().toString()}: INCAP NOT BLOCKED ${options.url}`)
      obs.next(data);
      obs.complete();
    }
  });
}

function checkForIncapsulaBust(data, options) {
  return Rx.Observable.create(obs => {
    if (JSON.stringify(data).toUpperCase().includes(`FROMCHARCODE`)) {
      log(options, `${Date().toString()}: INCAP NEEDS BUSTING ${options.url}`)
      const prefix = `${options.url.split(`.com`)[0]}.com`;
      const url = `${prefix}${incapsulaLink(data)}`;
      request({ url, headers, followRedirect: true }, (err, res, body) => {
        log(options, `${Date().toString()}: INCAP LINK SENT ${url}`)
        if (err) {
          log(options, "request error", err, options.url);
          obs.error();
        }
        request(options, (error, response, bod) => {
          log(options, `${Date().toString()}: REREQUEST ${options.url}`)
          if (err) {
            obs.error();
          }
          if (
            bod.toString().toUpperCase().includes(`JSTEST`) ||
            bod.toString().toUpperCase().includes(`INCAPSULA INCIDENT ID`)
          ) {
            log(options, `${Date().toString()}: INCAP REPEATED ON REREQUEST ${options.url}`)
            obs.error(bod);
          }
          obs.next(bod);
          obs.complete();
        });
      });
    } else {
      obs.next(data);
      obs.complete();
    }
  });
}
function export$(options) {
  return request$(options)
  //  .flatMap(data => checkForIncapsulaBlock(data, options))
    .retryWhen(error =>
      error
        .do(() => log(options, `${Date().toString()}: Incapsula Blocked, retry ${options.url}`))
        .delay(Math.floor(Math.random() * 5000) + 30000)
    )
    // .flatMap(data => checkForIncapsulaBust(data, options))
    // .retryWhen(error =>
    //   error
    //     .do(() => log(options, `${Date().toString()}: Incapsula Buster, retry ${options.url}`))
    //     .delay(Math.floor(Math.random() * 5000) + 3000)
    // )
    .do((x) => {
      if (typeof x === `string` && !options.url.includes("sales/list")) {
        log(options, `${Date().toString()} Returned Data: ${options.url}, Data: ${x.toString().substr(0, 10000)}`)
      }
    })
}


module.exports = export$;
