var Rx = require('@reactivex/rxjs');
var url = require('./copartUrls');
var request$ = require('./copartRequest');

var login$ =
  Rx.Observable.concat(
  //request$(url.copartFrontPage),
  request$(url.copartLogin)
  )
  //.do(x => console.log('login', x));

module.exports = login$;