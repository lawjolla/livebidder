var ws = require('../copartWebsocket/copartWebSocketStream').copartWs;
var Rx = require('@reactivex/rxjs');
var getPing = require('../copartWebsocket/copartWebSocketStream').getPing;
var dealer = require('../copartStores/dealerConstants');
var subscribedAuctions = require('../copartStores/copartSubscribedAuctionStore');
var url = require('./copartUrls');
var request$ = require('./copartRequest');
var auth$ = require('./copartBidToken');

function  subscribeToAuctionFeed(auction) {
  console.log('Copart Subscribe To auction: ', auction);
  Rx.Observable.concat(auth$, request$(url.copartAuctionLaneToken(auction)).map(x => JSON.parse(x).data))
    .scan((acc, cur) => {
      if (cur.hasOwnProperty('token')) {
        acc.auth = cur.token;
      }
      if (cur.hasOwnProperty('auctionToken')) {
        acc.auction = cur.auctionToken;
      }
      return acc;
    }, {auth: false, auction: false})
    .filter(x => x.auction !== false)
    .subscribe((tokens) => sendWebSocket(tokens, auction), (e) => console.log('Sub to Auction Feed Error: ', e))

}

function sendWebSocket(tokens, auction) {
  const yard = parseInt(auction.replace(/^\D+/g, ''));
  const lane = auction.slice(-1);
  subscribedAuctions.updateAuctionToken(tokens.auction, auction);
  const loginToLane = {
    "TOKEN":tokens.auction.toString(),"BUYERNUMBER": dealer.copart.username.toString(),
    "BUYERTOKEN": tokens.auth.toString(),"LANG":"en-US",
    "YARD": yard,"LANE":lane.toString(),"AUCTIONKEY":auction.toString(),
    "CLIENTTYPE":"macro","CMPCODE":"COPART","SITECODE":"C3US","DEVICENAME":"M2"
  };
  const encodedBidPacket = new Buffer(JSON.stringify(loginToLane)).toString('base64');
  ws.send('F=5&R=' + getPing() +'&E=1&N=/'+ auction +'/outbound,0,,F');
  const login = `F=14&R=${getPing()}&N=/${auction}/inbound&T=bG9naW4=&D=${encodedBidPacket}&L=0`;
  console.log(login);
  ws.send(login);
}

module.exports = subscribeToAuctionFeed;

