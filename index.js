const auctionProcesses = require('./auctionProcessStore');
var schedule = require('node-schedule');
// const request = require('./copart/copartAuth/copartRequest');
// const url = require('./copart/copartAuth/copartUrls');
// const headers = require('./copart/copartAuth/copartRequestHeaders');
//
// const cp = {
//   url: `https://www.copart.com`,
//   method: 'GET',
//   headers,
//   followAllRedirects: true,
// };
// request(cp)
//     .subscribe(x => console.log(x))


const times = {
  normalStart: '0 4 * * 1-5',
  normalStop: '0 17 * * 0-6',
  testStart: '27 11 * * 1-5',
  testStop: '28 11 * * 1-5'
};

const d = new Date();
const day = d.getDay();
const hour = d.getHours() + 1;
const startHour = Number(times.normalStart.split(' ')[1]);
const endHour = Number(times.normalStop.split(' ')[1]);

console.log(d.toString()+ ' Running!: ' + day + ' ' + hour);
//auctionProcesses.add(110554, 'republic06');
if (day > 0 && day < 6 && hour >= startHour && hour <= endHour ) {
  console.log(Date().toString() + ' Start Auctions');
  auctionProcesses.add(110554, 'Republic02');
}

var startAuctions = schedule.scheduleJob(times.normalStart, function(){
  console.log(Date().toString() + ' Start Auctions');
  auctionProcesses.add(110554, 'Republic02');
});

var stopAuctions = schedule.scheduleJob(times.normalStop, function(){
  console.log(Date().toString() + ' Kill Auctions');
  auctionProcesses.killAll();
});
